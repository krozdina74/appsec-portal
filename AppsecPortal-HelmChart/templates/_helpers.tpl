{{- define "appsecportal.secretsData" -}}
DB_PASS: {{ .Values.postgresql.auth.password | b64enc | quote }}
JWT_PRIVATE_KEY: {{ .Values.configs.secret.jwt_private_key | b64enc | quote }}
JWT_PUBLIC_KEY: {{ .Values.configs.secret.jwt_public_key | b64enc | quote }}
SECRET_KEY: {{ .Values.configs.secret.secret_key | b64enc | quote }}
{{- end -}}

{{- define "appsecportal.secret.amqp" -}}
{{- if .Values.rabbitmq.enabled }}
AMQP_HOST_STRING: {{ printf "amqp://%s:%s@%s:%s/" .Values.rabbitmq.auth.username .Values.rabbitmq.auth.password (include "rabbitmq.host" . ) .Values.rabbitmq.containerPorts.amqp | b64enc | quote }}
{{- else }}
AMQP_HOST_STRING: {{ printf "amqp://%s:%s@%s:%s/" .Values.rabbitmq.auth.username .Values.rabbitmq.auth.password .Values.rabbitmq.nameOverride .Values.rabbitmq.containerPorts.amqp | b64enc | quote }}
{{- end }}
{{- end -}}

{{- define "rabbitmq.host"}}
{{- printf "%s-%s" .Release.Name .Values.rabbitmq.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}    

{{- define "postgres.host"}}
{{- if (eq .Values.postgresql.enabled false) }}
{{- .Values.configs.configMap.database.host | quote }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Values.postgresql.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end }}
{{- end }}

{{/*
Create portal name
*/}}
{{- define "portal.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.portal.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of portal service account to use
*/}}
{{- define "portal.ServiceAccountName" -}}
{{- if .Values.portal.serviceAccount.create -}}
    {{ default (include "portal.fullname" .) .Values.portal.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.portal.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create auto-validator name
*/}}
{{- define "autoValidator.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.autoValidator.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of auto-validator service account to use
*/}}
{{- define "autoValidator.ServiceAccountName" -}}
{{- if .Values.autoValidator.serviceAccount.create -}}
    {{ default (include "autoValidator.fullname" .) .Values.autoValidator.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.autoValidator.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create db-helper name
*/}}
{{- define "dbhelper.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.dbhelper.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of db-helper service account to use
*/}}
{{- define "dbhelper.ServiceAccountName" -}}
{{- if .Values.dbhelper.serviceAccount.create -}}
    {{ default (include "dbhelper.fullname" .) .Values.dbhelper.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.dbhelper.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create dojo-helper name
*/}}
{{- define "dojohelper.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.dojohelper.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of dojo-helper service account to use
*/}}
{{- define "dojohelper.ServiceAccountName" -}}
{{- if .Values.dojohelper.serviceAccount.create -}}
    {{ default (include "dojohelper.fullname" .) .Values.dojohelper.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.dojohelper.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create importer name
*/}}
{{- define "importer.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.importer.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of importer service account to use
*/}}
{{- define "importer.ServiceAccountName" -}}
{{- if .Values.importer.serviceAccount.create -}}
    {{ default (include "importer.fullname" .) .Values.importer.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.importer.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
Create jira-helper name
*/}}
{{- define "jirahelper.fullname" -}}
{{- printf "%s-%s" (include "appsecportal.fullname" .) .Values.jirahelper.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of jira-helper service account to use
*/}}
{{- define "jirahelper.ServiceAccountName" -}}
{{- if .Values.jirahelper.serviceAccount.create -}}
    {{ default (include "jirahelper.fullname" .) .Values.jirahelper.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.jirahelper.serviceAccount.name }}
{{- end -}}
{{- end -}}
